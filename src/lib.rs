#![feature(proc_macro, wasm_custom_section, wasm_import_module)]

extern crate rand;
extern crate wasm_bindgen;

use wasm_bindgen::prelude::*;
use std::fmt;
use rand::{Rng, SeedableRng, XorShiftRng, distributions::{IndependentSample, Range}};

#[repr(u8)]
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Cell {
    Dead = 0,
    Alive = 1,
}

#[wasm_bindgen]
impl Cell {
    pub fn toggle(&mut self) {
        *self = match *self {
            Cell::Dead => Cell::Alive,
            Cell::Alive => Cell::Dead,
        };
    }
}

#[wasm_bindgen]
pub struct Universe {
    width: u32,
    height: u32,
    cells: Vec<Cell>,
}

impl Universe {
    fn get_index(&self, row: u32, column: u32) -> usize {
        (row * self.width + column) as usize
    }

    fn count_live_neighbors(&self, row: u32, column: u32) -> u8 {
        [self.height - 1, 0, 1]
            .iter()
            .cloned()
            .map(|delta_row| -> u8 {
                [self.width - 1, 0, 1]
                    .iter()
                    .cloned()
                    .map(|delta_col| {
                        if delta_row == 0 && delta_col == 0 {
                            0
                        } else {
                            let neighbor_row = (row + delta_row) % self.height;
                            let neighbor_col = (column + delta_col) % self.width;
                            let index = self.get_index(neighbor_row, neighbor_col);
                            self.cells[index] as u8
                        }
                    })
                    .sum()
            })
            .sum()
    }
}

impl fmt::Display for Universe {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for line in self.cells.as_slice().chunks(self.width as usize) {
            for &cell in line {
                let symbol = match cell {
                    Cell::Dead => "◻️",
                    Cell::Alive => "◼️",
                };
                write!(f, "{}", symbol)?;
            }
            write!(f, "\n")?;
        }

        Ok(())
    }
}

#[wasm_bindgen]
impl Universe {
    pub fn tick(&mut self) {
        self.cells = self.cells
            .iter()
            .enumerate()
            .map(|(i, cell)| {
                let row = i as u32 / self.width;
                let col = i as u32 % self.width;
                let live_neighbors = self.count_live_neighbors(row, col);

                match (*cell, live_neighbors) {
                    (Cell::Alive, x) if x < 2 => Cell::Dead,
                    (Cell::Alive, 2) | (Cell::Alive, 3) => Cell::Alive,
                    (Cell::Alive, x) if x > 3 => Cell::Dead,
                    (Cell::Dead, 3) => Cell::Alive,
                    (otherwise, _) => otherwise,
                }
            })
            .collect();
    }

    pub fn new(width: u32, height: u32) -> Universe {
        let cells = (0..(width * height))
            .map(|i| {
                if i % 2 == 0 || i % 7 == 0 {
                    Cell::Alive
                } else {
                    Cell::Dead
                }
            })
            .collect();

        Universe {
            width,
            height,
            cells,
        }
    }

    pub fn new_blank(width: u32, height: u32) -> Universe {
        let cells = (0..(width * height)).map(|_| Cell::Dead).collect();

        Universe {
            width,
            height,
            cells,
        }
    }

    pub fn new_full(width: u32, height: u32) -> Universe {
        let cells = (0..(width * height)).map(|_| Cell::Alive).collect();

        Universe {
            width,
            height,
            cells,
        }
    }

    pub fn new_random(width: u32, height: u32) -> Universe {
        let mut js_rng = JsRng::new();
        let seed_range = Range::new(0, u32::max_value());

        let mut rng = XorShiftRng::from_seed([
            seed_range.ind_sample(&mut js_rng),
            seed_range.ind_sample(&mut js_rng),
            seed_range.ind_sample(&mut js_rng),
            seed_range.ind_sample(&mut js_rng),
        ]);
        let coin = Range::new(0, 2);

        let cells = (0..(width * height))
            .map(|_| {
                let toss = coin.ind_sample(&mut rng);
                if toss == 1 {
                    Cell::Alive
                } else {
                    Cell::Dead
                }
            })
            .collect();

        Universe {
            width,
            height,
            cells,
        }
    }

    pub fn render(&self) -> String {
        self.to_string()
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn cells(&self) -> *const Cell {
        self.cells.as_ptr()
    }

    pub fn toggle_cell(&mut self, row: u32, column: u32) {
        let index = self.get_index(row, column);
        self.cells[index].toggle();
    }

    pub fn revive_cell(&mut self, row: u32, column: u32) {
        let index = self.get_index(row, column);
        self.cells[index] = Cell::Alive;
    }
}

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = Math)]
    fn random() -> f64;
}

struct JsRng {}

impl JsRng {
    fn new() -> JsRng {
        JsRng {}
    }
}

impl Rng for JsRng {
    fn next_u32(&mut self) -> u32 {
        let rand = random();
        (rand * u32::max_value() as f64) as u32
    }
}

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}
