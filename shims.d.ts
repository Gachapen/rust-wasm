declare module '*wasm_game_of_life_bg' {
    const memory: any;
    export {
        memory,
    };
}

declare module '*.vue' {
    import { VueConstructor } from 'vue';
    const component: VueConstructor;
    export default component;
}